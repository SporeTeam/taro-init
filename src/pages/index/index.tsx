import { Component } from 'react'
import { View, Text } from '@tarojs/components'
import { store, view } from '@risingstack/react-easy-state';

// 全局数据模型
import counter from "../../model/user";

import Hello from '../../components/hello/hello'

import { AtButton } from 'taro-ui'

import "taro-ui/dist/style/components/button.scss" // 按需引入
import './index.less'



// setInterval(()=>{
//   counter.increment();
// }, 10)

export default view(class Index extends Component {
  

  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  aaa(){
    console.log(this)
  }

  render () {
    return (
      <View className='index'>
        <Text>Hello world! {counter.num}</Text>
        <Hello></Hello>

        <Hello></Hello>
        <AtButton type='primary' onClick={this.aaa.bind(this)}>I need Taro UI</AtButton>
        <Text>Taro UI 支持 Vue 了吗？</Text>
        <AtButton type='primary' circle={true}>支持</AtButton>
        <Text>共建？</Text>
        <AtButton type='secondary' circle={true}>来</AtButton>
      </View>
    )
  }
})
