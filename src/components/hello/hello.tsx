import { Component } from 'react'
import { View, Text, Button } from '@tarojs/components'
import { AtButton } from 'taro-ui'
import { store, view } from '@risingstack/react-easy-state';

import counter from "../../model/user";


//https://github.com/RisingStack/react-easy-state


// const counter = store({
//   num: 0,
//   increment: () => counter.num++
// });


export default view(class Hello extends Component {

  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  render () {
    return (
      <AtButton type='primary' onClick={counter.increment}>{counter.num}</AtButton>
    )
  }
})
