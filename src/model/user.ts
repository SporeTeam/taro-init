import { store } from '@risingstack/react-easy-state';



let counter = store({
    num: 0,
    increment: () => counter.num++
});

export default counter;